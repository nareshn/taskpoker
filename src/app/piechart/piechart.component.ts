declare var Highcharts;

import { Component, OnInit, Input } from '@angular/core';
import { appConstants } from "../services/app.constants";
import { VoteService } from "../services/vote.service";

@Component({
    selector: 'app-piechart',
    templateUrl: './piechart.component.html',
    styleUrls: ['./piechart.component.scss']
})
export class PiechartComponent implements OnInit {

    @Input() mid: any;

    constructor(private VoteService: VoteService) { }

    ngOnInit() {
        this.VoteService.getVotes(this.mid).then((snapshot) => {
            let votes = snapshot.val();
            
            votes = Object.values(votes);

            votes = votes.reduce((mem, vote) => {
                if(!vote.value) {
                    vote.value = 0;
                } 
                mem[vote.value] = mem[vote.value] || {};
                let item = mem[vote.value];
                item.name = item.name || [];
                item.name.push(vote.name);
                item.y = item.y ? ++item.y : 1;
                item.val = vote.value;
                item.color = appConstants.colors[item.val || "0"];
                item.tooltip = this.getTooltip(item.val);
                return mem;
            }, []);

            votes = Object.values(votes).map((vote) => {
                vote["name"] = vote["name"].join();
                return vote;
            });

            votes.sort((a, b) => a.y - b.y);

            let colors = votes.map(vote => vote.color);
            this.draw(votes, colors);
        });
    }

    getTooltip(val) {
        let card = appConstants.cards.find(card => card.value === val);
        return card["tooltip"] || card.value;
    }

    draw(data, colors) {
        Highcharts.chart('container', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                backgroundColor: '#092138'
            },
            credits: {
                enabled: false
            },
            title: {
                text: 'Poker Result ',
                style: {
                    color: '#00f97b',
                    fontWeight: 'bold'
                }
            },
            tooltip: {
                pointFormat: '{point.y}'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    colors: colors,
                    dataLabels: {
                        enabled: true,
                        formatter: function(){
                            return `${this.point.y} vote` +  (this.point.y === 1 ? "" :"s") + ` for [${this.point.tooltip}]`;
                        },
                        style: {
                            color: "#fff",
                            fontSize: "15px"
                        }
                    }
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: data
            }]
        });
    }

}
