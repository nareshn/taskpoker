import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { IdService } from "../services/id.service"
import { LoginForm } from "./login-form"

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  adminForm: FormGroup;
  memberForm: FormGroup;
  isAdmin: boolean;

  constructor(private IdService: IdService, private router: Router, fb: FormBuilder) {
    this.adminForm = fb.group({
      mid: [null, Validators.compose([Validators.required, Validators.pattern('[0-9]{5}'), Validators.minLength(5), Validators.maxLength(5)])],
      uuid: '',
      name: [null, Validators.compose([Validators.required,Validators.pattern("[a-zA-Z ]*"), Validators.minLength(2), Validators.maxLength(5)])],
      type: ''
    });
    this.memberForm = fb.group({
      mid: [null, Validators.compose([Validators.required, Validators.pattern('[0-9]{5}'), Validators.minLength(5), Validators.maxLength(5)])],
      uuid: '',
      name: [null, Validators.compose([Validators.required, Validators.pattern("[a-zA-Z ]*"), Validators.minLength(2), Validators.maxLength(5)])],
      type: ''
    });

  }

  ngOnInit() {
    this.adminForm.setValue(Object.assign(new LoginForm(), this.IdService.getIds("admin")));
    this.memberForm.setValue(Object.assign(new LoginForm(), this.IdService.getIds("member")))
  }

  onJoin(loginForm: LoginForm) {
    let { name, mid, uuid, type } = loginForm;
    this.IdService.saveIds(loginForm);
    this.router.navigate([`${type}/${name}/${uuid}/${mid}`]);
  }

  onTab(isAdmin: boolean) {
    this.isAdmin = isAdmin;
  }

}