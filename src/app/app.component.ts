import { Component } from '@angular/core';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  today: any;
  constructor() {
    this.today = Date.now();
    setInterval(() => { this.today = new Date();}, 60 * 1000);
  }
  
}
