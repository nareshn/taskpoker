import { Injectable } from '@angular/core';

import {LoginForm} from "../login/login-form"

@Injectable()
export class IdService {

  constructor() { }

  saveIds(loginForm: LoginForm): void {
    let {type} = loginForm;
    window.localStorage.setItem(type,JSON.stringify(loginForm));
    window.localStorage.setItem("UUID",loginForm.uuid);
  }

  getIds(type): any {
     let ids = JSON.parse(window.localStorage.getItem(type)) || {};
     ids.type = type;
     ids.uuid = window.localStorage.getItem("UUID") || this.getRamdomUUID();
     return ids;
  }

  private getRamdomNumber(digits: number): string {
    return (Math.floor(Math.random()*90000) + 10000).toString();
  }

  private getRamdomUUID(): string {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

}
