declare var firebase;

import { Injectable } from '@angular/core';
import { LoginForm } from '../login/login-form';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class VoteService {
  database: any;
  constructor() {
    let config = {
      apiKey: "AIzaSyCZEfhNks_GgBPp58eH-CANiQvSRkJsSU4",
      authDomain: "taskpoker-6d9d4.firebaseapp.com",
      databaseURL: "https://taskpoker-6d9d4.firebaseio.com",
      projectId: "taskpoker-6d9d4",
      storageBucket: "taskpoker-6d9d4.appspot.com",
      messagingSenderId: "802934247946"
    };
    firebase.initializeApp(config);
    //firebase.database.INTERNAL.forceLongPolling();
    this.database = firebase.database();
    this.database.goOnline();
  }

  goOffline() {
    this.database.goOffline();
  }

  join(params: LoginForm): Promise<any> {
    if (params.type === "admin") {
      this.database.ref(params.mid + "/admin").set({
        name: params.name,
        type:"join",
        time: new Date().getTime()
      });
    }
    return this.database.ref(params.mid + "/votes/" + params.uuid).update({
        name: params.name,
        type: params.type,
        uuid: params.uuid,
        time: new Date().getTime()
      });
  }

  getVotes(mid) {
    return this.database.ref(mid + "/votes").once('value');
  }

  getAdminActions(mid) {
    return this.database.ref(mid + "/admin").once('value');
  }

  connectToVotes(params): Observable<any> {
    return new Observable(observer => {
      this.database.ref(params.mid + "/votes").on("value", snapshot => {
        snapshot && observer.next(snapshot.val());
      })
    });
  }

  connectToAdminActions(params): Observable<any> {
    return new Observable(observer => {
      this.database.ref(params.mid + "/admin").on("value", snapshot => {
        snapshot && observer.next(snapshot.val());
      })
    });
  }

  vote(params: LoginForm, value: number) {
    this.database.ref(params.mid + "/votes/" + params.uuid).update({ 
      name: params.name,
      type: params.type,
      uuid: params.uuid,
      value: value, 
      time: new Date().getTime() 
    });
  }

  resetVotes(params: LoginForm, voteCards: Array<any>) {
    voteCards = voteCards.reduce((mem, card) => {
      card.value = null;
      mem[card.uuid] = card;
      return mem;
    }, {});
    this.database.ref(params.mid + "/votes").set(voteCards);
    this.database.ref(params.mid + "/admin").update({ showVotes: false, type:"showVotes", time: new Date().getTime() });
  }

  showVotes(params: LoginForm) {
    this.database.ref(params.mid + "/admin").update({ showVotes: true, type:"showVotes" , time: new Date().getTime() });
  }

  removeAllMembers(params: LoginForm) {
    this.database.ref(params.mid + "/votes").remove();
  }

  removeMember(params: LoginForm, uuid: string) : Promise<any>{
    return this.database.ref(params.mid + "/votes/" + uuid).remove();
  }


}
