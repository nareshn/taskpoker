export const appConstants = {
    colors: {
        "-1": "#000000", 
        "-2": "#000000", 
        "0": "#10375d", "1": "#8d9dad", "2": "#4E5D6C", "3": "#4E5D6C",
        "5": "#028db7", "8": "#007761", "13": "#DF691A", "21": "#d9534f",
        "34": "#d9534f", "55": "#d9534f", "89": "#862825"
    },
    cards: [ {value: 1, label:"1"},{value: 2, label:"2"},{value: 3, label:"3"},
            {value: 5, label:"5"},{value: 8, label:"8"},{value: 13, label:"13"},
            {value: 21, label:"21"},{value: 34, label:"34"},{value: 55, label:"55"},
            {value: 89, label:"89"},{value: -1, label:"", class:"coffee", tooltip:"coffee"},
            {value: -2, class:"pass", label:"Pass", tooltip:"Pass"}, 
            {value: 0, class:"", label:"?", tooltip:"?"} ]
            
}