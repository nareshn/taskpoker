import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import {IdService} from "./services/id.service"
import {VoteService} from "./services/vote.service"

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PiechartComponent } from './piechart/piechart.component';


const appRoutes: Routes = [
  { path: ':type/:name/:uuid/:mid', component: DashboardComponent },
  { path: '',    component: LoginComponent },
  { path: '**', redirectTo: '', component: LoginComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    PiechartComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes,{ enableTracing: false })
  ],
  providers: [
    IdService,
    VoteService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
