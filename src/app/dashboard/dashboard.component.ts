import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';


import { LoginForm } from '../login/login-form';
import { VoteService } from "../services/vote.service";
import { appConstants } from "../services/app.constants";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {

  cards: Array<any>
  voteCards: Array<any>
  routeParams: LoginForm
  showVotes: boolean

  constructor(private route: ActivatedRoute, private router: Router, private VoteService: VoteService, private cd: ChangeDetectorRef) {
    this.cards = [...appConstants.cards].filter(card => card.value);
  }

  ngOnInit() {
    this.route.params.subscribe(params => this.routeParams = <LoginForm>params);
    this.validateRouteParams();

    this.VoteService.connectToVotes(this.routeParams).subscribe(this.listenToVotes.bind(this));
    this.VoteService.connectToAdminActions(this.routeParams).subscribe(this.listenToAdminActions.bind(this));

    this.VoteService.join(this.routeParams).then(() => {
      this.onRefresh();
    });
  }

  listenToVotes(votes) {
    /*votes = [{name: 'NN',value:8},{name: 'SDES',value:8},{name: 'POSS',value:8},{name: 'ASDED',value:5},
    {name: 'NAREH',value:5},{name: 'GH',value:5},{name: 'SJSJS',value:3},{name: 'GGFF',value:2},{name: 'SSSS',value:2},
    {name: 'SD'},{name: 'KJDF',value:21},{name: 'FDVGD',value:21},{name: 'NN',value:8},{name: 'SDES',value:8},{name: 'POSS',value:8},{name: 'ASDED',value:5},
    {name: 'NAREH',value:5},{name: 'GH',value:5},{name: 'SJSJS',value:3},{name: 'GGFF',value:2},{name: 'SSSS',value:2},
    {name: 'SD'},{name: 'KJDF',value:21},{name: 'FDVGD',value:21},{name: 'NN',value:8},{name: 'SDES',value:8},{name: 'POSS',value:8},{name: 'ASDED',value:5},
    {name: 'NAREH',value:5},{name: 'GH',value:5},{name: 'SJSJS',value:3},{name: 'GGFF',value:2},{name: 'SSSS',value:2},
    {name: 'SD'},{name: 'KJDF',value:21},{name: 'FDVGD',value:21}];*/

    this.voteCards = !votes ? [] : Object.values(votes);
    //this.hasVote = this.voteCards.find(card => card.uuid === this.routeParams.uuid);
    this.cd.detectChanges();
  }

  listenToAdminActions(actions) {
    this.showVotes = actions.showVotes;
    if (actions.type === "showVotes") {
      this.resetCards();
    }
    this.cd.detectChanges();
  }

  onCardSelect(value) {
    if (this.showVotes) {
      return;
    }
    this.cards.forEach(card => {
      card.selected = card.value === value ? true : false;
    });
    this.VoteService.vote(this.routeParams, value);
  }

  onFlipVote() {
    this.VoteService.showVotes(this.routeParams);
  }

  onResetVotes() {
    this.showVotes = false;
    this.VoteService.resetVotes(this.routeParams, this.voteCards);
    this.resetCards();
  }

  resetCards() {
    this.cards.forEach(card => { card.selected = false; });
  }

  onRemoveAll() {
    this.VoteService.removeAllMembers(this.routeParams);
  }

  onRemove(uuid): Promise<any> {
    return this.VoteService.removeMember(this.routeParams, uuid);
  }

  onRefresh() {
    this.VoteService.getVotes(this.routeParams.mid).then((snapshot) => {
      this.listenToVotes(snapshot.val());
    });
    this.VoteService.getAdminActions(this.routeParams.mid).then((snapshot) => {
      this.listenToAdminActions(snapshot.val());
    });
    this.cd.detectChanges();
  }

  onLogout() {
    this.onRemove(this.routeParams.uuid).then(()=>{
      this.VoteService.goOffline();
      //TODO fix this;
      window.location.href="https://taskpoker.com";
    });
  }

  isAdmin() {
    return this.routeParams.type === "admin"
  }

  validateRouteParams() {
    if (this.routeParams.type !== "admin" && this.routeParams.type !== "member") {
      this.router.navigate(['/']);
    }
    if (!this.routeParams.uuid
      || this.routeParams.uuid.length !== 36
      || !this.routeParams.mid
      || this.routeParams.mid.length !== 5
      || !this.routeParams.name
      || this.routeParams.name.length < 2
      || this.routeParams.name.length > 6) {
      this.router.navigate(['/']);
    }
  }

  getStyle(value, checkShowVotes) {
    if (!checkShowVotes || this.showVotes) {
      return { "background-color": appConstants.colors[value || 0] };
    }
    return { "background-color": appConstants.colors[0] };
  }

  ngOnDestroy() {

  }

}
